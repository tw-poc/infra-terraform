#!/bin/bash -xe
set -x
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
sudo yum update -y
sudo yum install git ruby wget jq -y
aws ec2 attach-volume --volume-id ${ebs_volume_id} --instance-id $(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id) --device /dev/xvdf --region=${aws_region}
sleep 20
mkdir /EBS
mount /dev/xvdf /EBS

# Install Docker
sudo amazon-linux-extras install docker
sudo systemctl start --now --no-block docker.service
sudo usermod -a -G docker ec2-user
sudo chkconfig docker on

# Install Java
sudo yum install java-${java_openjdk_version}-openjdk java-${java_openjdk_version}-openjdk-devel -y

# Install Jenkins
cd /opt
wget https://prodjenkinsreleases.blob.core.windows.net/redhat-stable/jenkins-${jenkins_lts_version}-1.1.noarch.rpm
rpm -ivh /opt/jenkins-${jenkins_lts_version}-1.1.noarch.rpm
sudo usermod -a -G docker -s /bin/bash jenkins
rm -rf /opt/jenkins-${jenkins_lts_version}-1.1.noarch.rpm

if [[ ! -d "/EBS/jenkins" ]]
then
  mv /var/lib/jenkins /EBS
  ln -s /EBS/jenkins /var/lib/jenkins
  chown -h jenkins.jenkins /var/lib/jenkins
else
  rm -rf /var/lib/jenkins
  ln -s /EBS/jenkins /var/lib/jenkins
  chown -h jenkins.jenkins /var/lib/jenkins
fi

sudo systemctl start --now --no-block jenkins.service

# Install & kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.19.0/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
runuser -l  jenkins -c 'aws eks update-kubeconfig --name ${eks_cluster} --region ap-south-1 --role-arn ${jenkins_role_arn}'
