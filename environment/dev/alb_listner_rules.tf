resource "aws_lb_listener_rule" "mediawiki_http" {

  listener_arn = module.alb-mediawiki.http_tcp_listener_arns[0]
  priority     = 1
  condition {
    path_pattern {
      values = ["/mediawiki*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = module.alb-mediawiki.target_group_arns[0]
  }

}
