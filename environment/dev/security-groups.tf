resource "aws_security_group" "alb_mediawiki" {
  name        = "${var.environment}-alb-sg"
  description = "Allow 80 to mediawiki ALB"
  vpc_id      = var.eks_vpc

  ingress {
    description = "Allowing 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "mediawiki_rds" {
  name        = "${var.environment}-mediawiki-rds-sg"
  description = "RDS Security group"
  vpc_id      = var.eks_vpc
}

resource "aws_security_group_rule" "igress_all_from_eks" {
    type = "ingress"
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    security_group_id = "${aws_security_group.mediawiki_rds.id}"
    source_security_group_id = var.eks_instance_sg
}
