variable "region" {
  default = "ap-south-1"
}

variable "profile" {
  default = "thoughtworks-poc"
}

variable "environment" {
  default = "dev"
}

variable "project" {
  default = "mediawiki"
}

variable "eks_vpc" {
  default = "vpc-01e59ff6cbf0b93a0"
}

variable "eks_pub_sub" {
  default = ["subnet-065457a2f6e9b7498", "subnet-045f52486897d65d6", "subnet-07e956dfeba4a756e"]
}

variable "eks_pvt_sub" {
  default = ["subnet-0c47975c025c37c80", "subnet-0c8cbb8d43800b8a8", "subnet-05ec5980131978dd7"]
}

variable "files_path" {
  default = "../../files/dev"
}

variable "db_username" {
  default = "mediawiki_user"
}

variable "db_password" {
  default = "Jg54Hd5465314156tyjh"
}

variable "eks_instance_sg" {
  default = "sg-00dfff02550f91493"
}
