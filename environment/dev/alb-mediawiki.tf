module "alb-mediawiki" {

  source                           = "../../modules/alb"
  name                             = "${var.environment}-alb-${var.project}"
  create_lb                        = true
  enable_cross_zone_load_balancing = true
  vpc_id                           = var.eks_vpc
  subnets                          = var.eks_pub_sub
  security_groups                  = ["${aws_security_group.alb_mediawiki.id}"]

  target_groups = [

    {
      "name"             = "mediawiki",
      "backend_protocol" = "HTTP",
      "backend_port"     = "30007",
      "target_type"      = "instance",
      "health_check" = {
        enabled                = true
        healthy_threshold   = 2
        interval            = 10
        matcher             = "200-302"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = 5
        unhealthy_threshold = 5
      }
    }

  ]

  http_tcp_listeners = [

    {
      "port"               = "80"
      "protocol"           = "HTTP"
      "target_group_index" = "0"
    }
  ]
}
