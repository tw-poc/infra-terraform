resource "aws_ecr_repository" "mediawiki" {
  name                 = "mediawiki"
  image_tag_mutability = "MUTABLE"
}
