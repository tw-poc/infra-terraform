module "mediawiki_rds_instance" {
  source  = "../../modules/rds"

  identifier = "${var.project}-${var.environment}-rds"

  engine            = "mysql"
  engine_version    = "5.7.26"
  instance_class    = "db.t3.small"
  allocated_storage = 5
  max_allocated_storage = 20
  storage_encrypted = true

  name     = "mediawiki"
  username = var.db_username
  password = var.db_password
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = ["${aws_security_group.mediawiki_rds.id}"]

  maintenance_window = "Thu:03:00-Thu:05:00"
  backup_window      = "06:00-07:00"
  backup_retention_period = "0"

  monitoring_interval = "0"
  monitoring_role_name = "RDSMonitoringRole"
  create_monitoring_role = false
  subnet_ids = var.eks_pvt_sub
  family = "mysql5.7"
  major_engine_version = "5.7"
  final_snapshot_identifier = "${var.project}-${var.environment}-mediawiki-final-snapshot"
  deletion_protection = true

  parameters = [
    {
      name = "character_set_client"
      value = "utf8"
    },
    {
      name = "character_set_server"
      value = "utf8"
    }
  ]
}
