variable "region" {
  default = "ap-south-1"
}

variable "profile" {
  default = "thoughtworks-poc"
}

variable "environment" {
  default = "jenkins"
}

variable "jenkins_cidr" {
  default = "10.100.0.0/16"
}

variable "jenkins_pub_sub" {
  default = ["10.100.1.0/24", "10.100.2.0/24", "10.100.3.0/24"]
}

variable "jenkins_ami_id" {
  default = "ami-0447a12f28fddb066"
}

variable "jenkins_lts_version" {
  default = "2.204.6"
}

variable "java_openjdk_version" {
  default = "1.8.0"
}

variable "jenkins_instance_type" {
  default = "t2.large"
}

variable "files_path" {
  default = "../../files/jenkins"
}

variable "eks_cluster" {
  default = "mediawiki"
}
