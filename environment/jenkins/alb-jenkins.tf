module "alb-jenkins" {

  source                           = "../../modules/alb"
  name                             = "${var.environment}-alb"
  create_lb                        = true
  enable_cross_zone_load_balancing = true
  vpc_id                           = module.vpc.vpc_id
  subnets                          = module.vpc.public_subnets[*]
  security_groups                  = ["${aws_security_group.alb_jenkins.id}"]

  target_groups = [

    {
      "name"             = "jenkins",
      "backend_protocol" = "HTTP",
      "backend_port"     = "8080",
      "target_type"      = "instance",
      "health_check" = {
        enabled                = true
        healthy_threshold   = 2
        interval            = 30
        matcher             = "403"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = 10
        unhealthy_threshold = 5
      }
    }

  ]

  http_tcp_listeners = [

    {
      "port"               = "80"
      "protocol"           = "HTTP"
      "target_group_index" = "0"
    }
  ]
}
