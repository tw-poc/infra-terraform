data "template_file" "jenkins_ec2_policy" {
  template = file("${var.files_path}/jenkinsec2_policy.json")
}

resource "aws_iam_policy" "jenkins_ec2_policy" {
  name        = "${var.environment}-jenkins-policy"
  description = "Allowing full access"
  policy      = "${data.template_file.jenkins_ec2_policy.rendered}"
}

resource "aws_iam_role_policy_attachment" "jenkins_ec2_role_attachment" {
  role       = "${aws_iam_role.jenkins_assume_role.name}"
  policy_arn = "${aws_iam_policy.jenkins_ec2_policy.arn}"
}

resource "aws_iam_role" "jenkins_assume_role" {
  name               = "${var.environment}-jenkins"
  assume_role_policy = file("${var.files_path}/jenkinsec2_role.json")
}

resource "aws_iam_instance_profile" "jenkins_profile" {
  name = "${var.environment}-jenkins"
  role = "${aws_iam_role.jenkins_assume_role.name}"
}

data "template_file" "jenkins_userdata" {
  template = file("${var.files_path}/jenkins_userdata.sh")
  vars = {
    aws_region           = var.region
    ebs_volume_id        = "${aws_ebs_volume.jenkins.id}"
    jenkins_lts_version  = var.jenkins_lts_version
    java_openjdk_version = var.java_openjdk_version
    jenkins_role_arn     = "${aws_iam_role.jenkins_assume_role.arn}"
    eks_cluster          = var.eks_cluster
  }
}


module "jenkins-asg" {


  source   = "../../modules/asg"
  name     = "${var.environment}"
  lc_name  = "${var.environment}-jenkins"
  asg_name = "jenkins"

  image_id             = var.jenkins_ami_id
  instance_type        = var.jenkins_instance_type
  iam_instance_profile = aws_iam_instance_profile.jenkins_profile.arn
  user_data            = data.template_file.jenkins_userdata.rendered

  target_group_arns         = ["${module.alb-jenkins.target_group_arns[0]}"]
  security_groups           = ["${aws_security_group.jenkins_ec2.id}"]
  vpc_zone_identifier       = ["${module.vpc.public_subnets[0]}"]
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0

  root_block_device = [
    {
      volume_size = "20"
      volume_type = "gp2"
      encrypted   = true
    }
  ]
}
