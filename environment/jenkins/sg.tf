resource "aws_security_group" "alb_jenkins" {
  name        = "${var.environment}-jenkins-alb-sg"
  description = "Allow 80 to Jenkins ALB"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    description = "Allowing 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "jenkins_ec2" {
  name        = "${var.environment}-jenkins-ec2-sg"
  description = "Jenkins EC2 Sg"
  vpc_id      = "${module.vpc.vpc_id}"
}

resource "aws_security_group_rule" "igress_all_jenkins_ec2" {
    type = "ingress"
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    security_group_id = "${aws_security_group.jenkins_ec2.id}"
    source_security_group_id = "${aws_security_group.alb_jenkins.id}"
}

resource "aws_security_group_rule" "egress_all_jenkins_ec2" {
    type = "egress"
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    security_group_id = "${aws_security_group.jenkins_ec2.id}"
    cidr_blocks = ["0.0.0.0/0"]
}
