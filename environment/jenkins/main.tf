provider "aws" {
  region  = var.region
  profile = var.profile
}

terraform {
  backend "s3" {

    bucket  = "tw-poc"
    key     = "jenkins/terraform.tfstate"
    region  = "ap-south-1"
    profile = "thoughtworks-poc"
  }
}
