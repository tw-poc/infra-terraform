resource "aws_lb_listener_rule" "jenkins_http" {

  listener_arn = module.alb-jenkins.http_tcp_listener_arns[0]
  priority     = 101
  action {
    type             = "forward"
    target_group_arn = module.alb-jenkins.target_group_arns[0]
  }

  condition {
    path_pattern {
      values = ["*"]
    }
  }
}
