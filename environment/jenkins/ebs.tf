resource "aws_ebs_volume" "jenkins" {
  availability_zone = "${data.aws_availability_zones.available_zone.names[0]}"
  size              = 50
}
