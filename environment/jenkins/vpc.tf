module "vpc" {

  source          = "../../modules/vpc"
  name            = var.environment
  cidr            = var.jenkins_cidr
  azs             = data.aws_availability_zones.available_zone.names[*]
  public_subnets  = var.jenkins_pub_sub

  enable_nat_gateway = false

}
