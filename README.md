This is the Terraform code repository for hosting the mediawiki application infrastructure https://gitlab.com/jithindevasia/mediawiki

**Terraform structure:**

1. **./environment**
Each environment is separated under this directory. This setup have two environments, which are jenkins & dev. The jenkins environment is to host the Jenkins server and its related components. Dev environment contains the actual EKS cluster and other components. Both these environments are hosted in two different VPCs under the same AWS account. Ideally it should go into two different accounts. But this is just for demo.

   - /dev 
   - /jenkins

* main.tf - This file contains the Terraform user profile & region configurations. No access keys are hard coded and instead the main.tf expects a region & aws profile name. The region is mentioned in the varaibles.tf and AWS CLI profile name too. 
* Terraform state file is stored in S3 and this S3 bucket is not managed by this Terraform since the Terraform expects this S3 to be created already. 
* The varaibles are defined under varaibles.tf in respective environment directory.

2. **./files**
This directory contains the files for respective environments. Eg; policy json files, user-data scripts, s3 objects etc.

3. **./modules**
The modules directory contains several different Terraform modules for VPC, Load balancers, ASG etc. These are upstream Terraform modules provided by Terraform.

**How to use this Terraform repository ?**

Note: Please make sure that your IAM user has enough permission on this AWS account to create several resources. Prefer to have admin access. 

1. Download **Terraform v0.12.8** from https://www.terraform.io/downloads.html
2. Download and configure AWS cli version **aws-cli/1.16.303**
3. Configure AWS cli on your local machine with your IAM user access keys & secret keys under profile **thoughtworks-poc**
4. Clone this repository into your local machine.
5. cd into environment/jenkins or environment/dev
6. run **terraform init**
7. run **terraform plan**
8. run **terraform apply**
